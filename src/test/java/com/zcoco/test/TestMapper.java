package com.zcoco.test;

import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.zcoco.dao.UserMapper;
import com.zcoco.model.User;

/**
 * 功能说明：<br/>
 * 测试类
 * </br>
 * 开发人员：zhengkk(zhengkk@strongit.com.cn)<br/>
 * 开发时间：2015年7月7日<br/>
 */
public class TestMapper {

    @Test
    public void testMapper() {
        System.out.println("Hello ");
        ApplicationContext ac = new ClassPathXmlApplicationContext("applicationContext-mybatis.xml");
        UserMapper sd = (UserMapper) ac.getBean("userMapper");
        User user = new User();
        user.setUid("zcoco");
        sd.insert(user);
    }

}
