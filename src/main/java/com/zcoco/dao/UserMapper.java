package com.zcoco.dao;

import org.springframework.stereotype.Repository;

import com.zcoco.model.User;

@Repository
public interface UserMapper {

    /**
     * 添加新用户
     *
     * @param user
     * @return
     */
    public int insert(User user);

}
