package com.zcoco.authority.realm;

import org.apache.log4j.Logger;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;

import com.zcoco.dao.UserMapper;

/**
 * 功能说明：<br/>
 * 自定义DefineAuthorizingRealm
 * </br>
 * 开发人员：zhengkk(zhengkk@strongit.com.cn)<br/>
 * 开发时间：2015年7月8日<br/>
 */
public class DefineAuthorizingRealm extends AuthorizingRealm {

    private static Logger logger = Logger.getLogger(DefineAuthorizingRealm.class);

    @Autowired
    private UserMapper userMapper;

    public void setUserMapper(UserMapper userMapper) {
        this.userMapper = userMapper;
    }

    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {

        return null;
    }

    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
        return null;
    }

    /**
     * 将一些数据放到ShiroSession中,以便于其它地方使用
     * @see  比如Controller,使用时直接用HttpSession.getAttribute(key)就可以取到
     */
    @SuppressWarnings("unused")
    private void setSession(Object key, Object value) {
        Subject currentUser = SecurityUtils.getSubject();
        if (null != currentUser) {
            Session session = currentUser.getSession();
            System.out.println("Session默认超时时间为[" + session.getTimeout() + "]毫秒");
            if (null != session) {
                session.setAttribute(key, value);
            }
        }
    }

}
