package com.zcoco.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zcoco.dao.UserMapper;
import com.zcoco.model.User;
import com.zcoco.service.UserService;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserMapper userMapper;


    @Override
    public int insertUser(User user) {
        int i = userMapper.insert(user);
        return i;
    }

}
