package com.zcoco.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.zcoco.model.User;
import com.zcoco.service.UserService;

@Controller
public class RestConstroller {

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/login/{user}", method = RequestMethod.GET)
    public ModelAndView myMethod(HttpServletRequest request, HttpServletResponse response, @PathVariable("user") String user, ModelMap modelMap)
            throws Exception {
        modelMap.put("loginUser", user);
        User users = new User();
        users.setUid("asdzc111c");
        users.setState("1");
        users.setUsername("zcoco");
        userService.insertUser(users);

        return new ModelAndView("/hello", modelMap);
    }

    @RequestMapping(value = "/welcome", method = RequestMethod.GET)
    public String registPost() {
        return "/welcome";
    }

}
