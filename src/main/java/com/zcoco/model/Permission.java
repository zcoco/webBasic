package com.zcoco.model;

import java.io.Serializable;

/**
 * 功能说明：<br/>
 * 权限
 * </br>
 * 开发人员：zhengkk(zhengkk@strongit.com.cn)<br/>
 * 开发时间：2015年7月8日<br/>
 */
public class Permission implements Serializable {

    private static final long serialVersionUID = 1927123850523329770L;

    private String uid;

    private String name;

    public Permission() {
        super();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((uid == null) ? 0 : uid.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Permission other = (Permission) obj;
        if (uid == null) {
            if (other.uid != null)
                return false;
        } else if (!uid.equals(other.uid))
            return false;
        return true;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
